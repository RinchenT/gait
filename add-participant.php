<?php
session_start ();
include 'php/api/check.php';
include 'php/api/main/upload.php';
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/index.css?'?ver=0.3'">
<link rel="stylesheet" type="text/css" href="css/navbar.css?'?ver=0.5'">
<link rel="stylesheet" type="text/css" href="css/patients.css?ver=0.2'">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalabel=no">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<title>Patients</title>
</head>
<body>
	
   <?php include 'php/navbar.php';?>
   <!DOCTYPE html>

	<div id="main">
		<div class="main-head">
			<a href="index.php">Dashboard</a> / Participant
		</div>
		<div class="participants">
			<div class="paricipants-header">
				<a class="addBtn" href="participant.php"><i
					class="fa fa-fw  fa-list"></i>Participants</a>
			</div>
			<form action="php/Api/patient.php" id="add-participant" method="POST"
				role="form" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6">
						<label class="name">First Name</label> <br> <input
							type"text" name="fname" class="fname"
							placeholder="Enter First Name " required><br> <label class="name">Last
							Name</label> <br> <input type"text" name="sname" class="lname"
							placeholder="Enter Last Name" required><br> <label class="name">Date
							of Birth</label><br> <input type="date" name="dob" class="dob"
							placeholder="Date of Birth" required><br> <label class="name">Sex</label><br>
						<input type="radio" name="gender" value="male" checked> Male <input
							type="radio" name="gender" value="female"> Female <input
							type="radio" name="gender" value="other"> Other<br> <br> <label
							class="name">Angle Data</label> <br> <input type="file"
							name="angleFile" id="upload-file"><br> <label class="name">Moment
							Data</label> <br> <input type="file" name="momentFile"
							id="upload-file"><br> <label class="name">Power Data</label> <br>
						<input type="file" name="powerFile" id="upload-file"><br> <label
							class="name">CoP</label> <br> <input type="file" name="copFile"
							id="upload-file"><br> <label class="name">Force XYZ</label> <br>
						<input type="file" name="forceFile" id="upload-file">
					</div>
					<div class="col-md-6">

						<div class="comment">
							<lable class="name">Description</lable>
							<br>
							<textarea class="description" name="description"
								form="add-participant" placeholder="Enter a Description"
								required></textarea>
							<p style="font-size: 12px; font-weight: 450;">
								If there is no description please insert <b>N/A</b> instead!
							</p>
						</div>
					</div>
				</div>
<br/>
				<p class=" text-danger" style="font-size: 14px;">Please ensure that
					the mean data have been removed from the files!</p>

				<button type="cancel" class="cancelBtn"
					onclick="window.location='participant.php';return false;">Cancel</button>
				<input type="submit" class="saveBtn" name="submit" value="Save">
			</form>
		</div>

	</div>
</body>