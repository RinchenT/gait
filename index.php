<?php
session_start();
include 'php/api/check.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "gait";
			
$conn = new mysqli($servername, $username, $password, $dbname);
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/navbar.css?'?ver=0.8'">
	<meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Gait</title>
</head>
<body>
   <?php include 'php/navbar.php';?>
   <div id="main">
		<div class="main-head">
			<a href="index.php">Dashboard </a> / My Dashboard
		</div>
		<div class="container dashboard-container">
			<div class="row" style="margin-left: 5px;">
				
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white o-hidden h-100" style="background-color: #31bca5;">
						<div class="card-body">
							<div class="card-body-icon">
								<i class="fa fa-fw fa-user-md card-icons"></i>
							</div>
							<div class="mr-5">
							<?php 
							$sql = "SELECT COUNT(ID) FROM patients WHERE timecreated > NOW() - INTERVAL 1 WEEK";
							$result = $conn->query($sql);
							$amountOfPatients = array_values($result->fetch_assoc())[0];
							
							echo $amountOfPatients . ' New Participants This Week!';
							?>
							</div>
						</div>
						</a>
					</div>
				</div>
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white o-hidden h-100" style="background-color: #379ad3;">
						<div class="card-body">
							<div class="card-body-icon">
								<i class="fa fa-fw fa-support card-icons"></i>
							</div>
							<div class="mr-5">
							<?php 
							$sql = "SELECT COUNT(ID) FROM patients";
							$result = $conn->query($sql);
							$amountOfPatients = array_values($result->fetch_assoc())[0];
								
							echo $amountOfPatients . ' Total Participants!';
							?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card mb-3">
				<div class="card-header">
					Area Chart Example
				</div>
				<div class="card-body">
					<div class="chartjs-size-monitor"
						style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
						<div class="chartjs-size-monitor-expand"
							style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
							<div
								style="position: absolute; width: 1000000px; height: 1000000px; left: 0; top: 0"></div>
						</div>
						<div class="chartjs-size-monitor-shrink"
							style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
							<div
								style="position: absolute; width: 200%; height: 200%; left: 0; top: 0"></div>
						</div>
					</div>
					<canvas id="myAreaChart" width="941" height="282"
						class="chartjs-render-monitor"
						style="display: block; width: 941px; height: 282px;"></canvas>
				</div>
				<div class="card-footer small text-muted">Updated yesterday at 11:59
					PM</div>
			</div>
		</div>
	</div>
</body>
</html>