<div id="top-nav">
	<div onclick="openMobileMenu()" class="mobile-nav">
    		<div class="bar"></div>
    		<div class="bar"></div>
    		<div class="bar"></div>
    </div>
	<a href="index.php">
    	<div class="logo">
    		<h4 style="color: #fff">Gait Analsysis</h4>
    	</div>
	</a>
	<a class="account" onclick="openLogout()" title="Profile"><i class="fa fa-user"></i><i class="fa fa-sort-down"></i></a>
	</div>
</div>
<div id="left-nav">
	<div id="profile">
        <h6 style="text-align: center; color: #000" >Welcome</h6>
        <h6 style="text-align: center;"><?= $_SESSION["user"]["first_name"]?></h6>
    </div>
	<ul>
		<a href="index.php"><li><i class="fa fa-fw  fa-th-large"></i><span>Dashboard</span></li></a>
		<a href="charts.php"><li><i class="fa fa-fw  fa-area-chart"></i><span>Charts</span></li></a>
		<a href="participant.php"><li><i class="fa fa-fw fa-users"></i><span>Participant</span></li></a>
	</ul>
</div>
<div class="profile-container">
    <div class="profile-nav">
    	<ul>
    		<a href="profile.php?<?= $_SESSION['user']['username'] ?>"><li><i class="fa profile-nav-icons fa-user"></i>Profile</li></a>
    		<li class="divider"></li>
    		<a href="php/Api/logout.php"><li><i class="fa profile-nav-icons fa-sign-out"></i>Logout</li></a>
    	</ul>
    </div>
</div>

</script>
<script type="text/javascript">
function openLogout(){
        $(".profile-nav").toggle();
}

function openSettingst(){
    $(".settings").show();
}
function openMobileMenu(){
    $("#left-nav").animate({
      width: "toggle"
    });
}
</script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>