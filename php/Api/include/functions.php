<?php
class Functions {
	private $conn;
	
	// constructor
	function __construct() {
		require 'conn.php';
		// connecting to database
		$db = new Connect ();
		$this->conn = $db->connect ();
	}
	
	// destructor
	function __destruct() {
	}
	
	/**
	 * Storing new user
	 * returns user details
	 */
	public function storeUser($first_name, $last_name, $username, $password) {
		$uuid = uniqid ( '', true );
		$first_name = $_POST ['first_name'];
		$last_name = $_POST ['last_name'];
		$hash = $this->hashSSHA ( $password );
		$encrypted_password = $hash ["encrypted"]; // encrypted password
		$salt = $hash ["salt"]; // salt
		
		$stmt = $this->conn->prepare ( "INSERT INTO users(unique_id, first_name, last_name, username, encrypted_password, salt, created_at) VALUES(?, ?, ?, ?, ?, ?, NOW())" );
		$stmt->bind_param ( "ssssss", $uuid, $first_name, $last_name, $username, $encrypted_password, $salt );
		$result = $stmt->execute ();
		$stmt->close ();
		
		// check for successful store
		if ($result) {
			$stmt = $this->conn->prepare ( "SELECT * FROM users WHERE username = ?" );
			$stmt->bind_param ( "s", $username );
			$stmt->execute ();
			$user = $stmt->get_result ()->fetch_assoc ();
			$stmt->close ();
			
			return $user;
		} else {
			return false;
		}
	}
	
	/**
	 * Get user by username and password
	 */
	public function getUserByUsernameAndPassword($username, $password) {
		$stmt = $this->conn->prepare ( "SELECT * FROM users WHERE username = ?" );
		
		$stmt->bind_param ( "s", $username );
		
		if ($stmt->execute ()) {
			$user = $stmt->get_result ()->fetch_assoc ();
			$stmt->close ();
			
			// verifying user password
			$salt = $user ['salt'];
			$encrypted_password = $user ['encrypted_password'];
			$hash = $this->checkhashSSHA ( $salt, $password );
			// check for password equality
			if ($encrypted_password == $hash) {
				// user authentication details are correct
				return $user;
			}
		} else {
			return NULL;
		}
	}
	
	/**
	 * Check user is existed or not
	 */
	public function isUserExisted($username) {
		$stmt = $this->conn->prepare ( "SELECT username from users WHERE username = ?" );
		
		$stmt->bind_param ( "s", $username );
		
		$stmt->execute ();
		
		$stmt->store_result ();
		
		if ($stmt->num_rows > 0) {
			// user existed
			$stmt->close ();
			return true;
		} else {
			// user not existed
			$stmt->close ();
			return false;
		}
	}
	
	/**
	 * Encrypting password
	 * 
	 * @param
	 *        	password
	 *        	returns salt and encrypted password
	 */
	public function hashSSHA($password) {
		$salt = sha1 ( rand () );
		$salt = substr ( $salt, 0, 10 );
		$encrypted = base64_encode ( sha1 ( $password . $salt, true ) . $salt );
		$hash = array (
				"salt" => $salt,
				"encrypted" => $encrypted 
		);
		return $hash;
	}
	
	/**
	 * Decrypting password
	 * 
	 * @param
	 *        	salt, password
	 *        	returns hash string
	 */
	public function checkhashSSHA($salt, $password) {
		$hash = base64_encode ( sha1 ( $password . $salt, true ) . $salt );
		
		return $hash;
	}
	
	/**
	 * CREATING A NEW PATIENT
	 */
	public function storePatient($fname, $sname) {
		// User Details
		$first_name = $_POST ['fname'];
		$last_name = $_POST ['sname'];
		$date_of_birth = $_POST ['dob'];
		$gender = $_POST ['gender'];
		$description = $_POST ['description'];
		
		// File Details
		$angle_file_name = $_FILES ['angleFile'] ['name'];
		$angle_file_tmp = $_FILES ['angleFile'] ['tmp_name'];
		
		$newAngleFile = $this->storeFile ( $angle_file_name, $angle_file_tmp );
		
		$moment_file_name = $_FILES ['momentFile'] ['name'];
		$moment_file_tmp = $_FILES ['momentFile'] ['tmp_name'];
		$newMomentFile = $this->storeFile ( $moment_file_name, $moment_file_tmp );
		
		$power_file_name = $_FILES ['powerFile'] ['name'];
		$power_file_tmp = $_FILES ['powerFile'] ['tmp_name'];
		$newPowerFile = $this->storeFile ( $power_file_name, $power_file_tmp );
		
		$patientUniqueId = uniqid ( '', true );
		
		$stmt = $this->conn->prepare ( "INSERT INTO patients(fname, unique_id, sname, description, angle_file, moment_file, power_file, dob, gender) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)" );
		$stmt->bind_param ( "sssssssss", $first_name, $patientUniqueId, $last_name, $description, $newAngleFile, $newMomentFile, $newPowerFile, $date_of_birth, $gender );
		$result = $stmt->execute ();
		
		if ($result) {
			echo "SUCCESS";
		} else {
			echo "FAIL";
		}
		
		$stmt->close ();
	}
	public function storeFile($fileName, $fileTemp) {
		$fileExt = explode ( '.', $fileName );
		$fileActualExt = strtolower ( end ( $fileExt ) );
		$fileNameNew = uniqid ( '', true ) . "." . $fileActualExt;
		$fileDestination = "../../patients/" . $fileNameNew;
		move_uploaded_file ( $fileTemp, $fileDestination );
		
		if($fileName == null){
			return null;
		} else {
			return $fileNameNew;
		}
		
	}
	public function getAllPatients() {
		$result = $this->conn->prepare ( "SELECT id, fname, sname FROM patients" );
		
		while ( $row = $result->fetch_assoc () ) {
			echo "id: " . $row ["id"] . " - Name: " . $row ["fname"] . " " . $row ["sname"] . "<br>";
		}
	}
	public function getPatient($id) {
		$result = $this->conn->prepare ( "SELECT id, unique_id, fname, sname FROM patients WHERE unique_id = $id" );
		
		echo $result;
	}
	public function updateAccount($first_name, $last_name, $position, $email, $newpassword) {
		$uname = $_SESSION ['user'] ['id'];
		$sql = "UPDATE users SET first_name, last_name, position, email = '$first_name', '$last_name', '$position', '$email' WHERE id = ? ";
		$stmt = $this->conn->prepare ( $sql );
		$stmt->bind_param ( "s", $uname );
		$result = $stmt->execute ();
		
		if (mysqli_query ( $this->conn, $sql )) {
			echo "Record updated successfully";
		} else {
			echo "Error updating record: " . mysqli_error ( $this->conn );
		}
		
		if ($result) {
			$stmt = $this->conn->prepare ( "SELECT * FROM users WHERE id = ?" );
			$stmt->bind_param ( "s", $uname );
			$stmt->execute ();
			$user = $stmt->get_result ()->fetch_assoc ();
			$stmt->close ();
			var_dump ( $user );
			return $user;
		} else {
			return false;
		}
	}
}
?>