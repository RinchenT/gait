<?php
session_start();

require_once 'include/Functions.php';
$db = new Functions();

// json response array

if (isset($_POST['username']) && isset($_POST['password'])) {
	
	// receiving the post params
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	// get the user by email and password
	$user = $db->getUserByUsernameAndPassword($username, $password);
	
	if ($user != false) {
		//user is found 
		$_SESSION['loggedin'] = true;
		$_SESSION['user'] = $user;
		//start session() and redirect to homepage.
		header("Location: ../../index.php");
	} else {
		// user is not found with the credentials
		$_SESSION['Error'] = "Username or Password is Incorrect. Please try again!";
		header("Location:../../login.php");
	}
} 
?>