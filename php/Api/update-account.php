<?php
session_start();
require_once 'include/functions.php';
$db = new Functions();

if (isset($_POST['first_name'] || isset($_POST['last_name']) || isset($_POST['position'])) || isset($_POST['email'])) {
        // receiving the post params
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $position = $_POST['position'];
        $email = $_POST['email'];
        
        $_SESSION['Approved'] = "Account updated";
       // create a new user
        header("Location:../../edit-account.php");
        $user = $db->updateAccount($first_name, $last_name, $position,$email, $newpassword);
         if ($user != NULL) {
              // user stored successfully
            $_SESSION['user'] = $user;
         } else {
             // user failed to store
            $_SESSION['Error'] = "Unknown error occurred! Please try again!";
            header("Location:../../edit-account.php");
               
         }
} else {
    $_SESSION['Error'] = "Nothing has been changed!";
    header("Location:../../edit-account.php");
}

