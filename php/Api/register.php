<?php
session_start();
require_once 'include/functions.php';
$db = new Functions();

if (isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2'])) {
	if($_POST['password'] == $_POST['password2']){
	
	// receiving the post params
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	$_SESSION['Approved'] = "Account successfully created";
	header("Location:../../login.php");
	// check if user is already existed with the same email
	if ($db->isUserExisted($username)) {
		// user already existed
		$_SESSION['Error'] = "Username ".$username."  already has an account with Gait";
		header("Location:../../register.php");
	} else {
		// create a new user
		$user = $db->storeUser($first_name, $last_name, $username, $password);
		if ($user) {
			// user stored successfully
			echo $user["unique_id"];
			echo $user["first_name"];
			echo $user["last_name"];
			echo $user["username"];
			echo $user["created_at"];
			echo $user["updated_at"];
		} else {
			// user failed to store
			$_SESSION['Error'] = "Unknown error occurred in registration!";
			header("Location:../../register.php");
		}
	}
	} else{
		$_SESSION['Error'] = "Password does not match";
		header("Location:../../register.php");
	}
} else {
	$_SESSION['Error'] = "Fields not filled in!";
	header("Location:../../register.php");
}
?>