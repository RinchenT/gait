<?php
session_start ();
include 'php/api/check.php';
include 'php/api/main/upload.php';


?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/index.css?'?ver=0.3'">
<link rel="stylesheet" type="text/css" href="css/navbar.css?'?ver=0.7'">
<link rel="stylesheet" type="text/css" href="css/profile.css?ver=0.1'">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<title>Participants</title>
</head>
<body>
	
   <?php include 'php/navbar.php';?>
   <!DOCTYPE html>

	<div id="main">
		<div class="profile_wrap">
			<form action="php/api/update-account.php" method="POST">
				<div class="row edit-row">
					<div class="col-md-6">
						<br>
						<h4>Page information</h4>
						<div class="entry">
							<span>First Name</span><br>
							<i class="fa user fa-user"></i><input name="first_name" class="name" value="<?php echo $_SESSION['user']['first_name']?>">
						</div>
						<div class="entry">
							<span>Last Name</span><br>
							<i class="fa user fa-user"></i><input name="last_name" class="name" value="<?php echo $_SESSION['user']['last_name']?>">
						</div>
						<div class="entry">
							<span>Position</span><br>
							<i class="fa user fa-briefcase"></i><input name="position" class="name" value="<?php echo $_SESSION['user']['position']?>">
						</div>
						<div class="entry">
							<span>E-mail</span><br>
							<i class="fa user fa-envelope"></i><input name="email" class="name" value="<?php echo $_SESSION['user']['email']?>">
						</div>
						<button type="cancel" class="cancelBtn" onclick="window.location='profile.php?<?=$_SESSION['user']['username']?>';return false;">Cancel</button>
						<input type="submit" class="saveBtn" name="submit" value="Save">
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>