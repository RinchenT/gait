<?php
session_start ();
include 'php/api/check.php';

$participantId = $_SERVER ['QUERY_STRING'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "gait";

$conn = new mysqli ( $servername, $username, $password, $dbname );

?>

<html>
<head>
<title>Charts</title>
<link rel="stylesheet" type="text/css" href="css/index.css?'?ver=0.2'">
<link rel="stylesheet" type="text/css" href="css/navbar.css?'?ver=0.5'">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<script async="" src="//www.google-analytics.com/analytics.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
	integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--Load the AJAX API-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>
<script type="text/javascript" src="javascript/charts.js"></script>
</head>

<body>

	<div class="navbar-here">
    <?php include 'php/navbar.php'?>
    </div>
	<div id="main">
		<div class="main-head">
			<a href="index.php">Dashboard</a> / Recent Chart Analytics
		</div>

		<!--Div that will hold the pie chart-->

		<div class="container charts-wrapper">
		<!-- 
			<button class="backBtn" onclick="window.location='participant.php'">
				<i class="fa back-arrow fa-arrow-left"></i>Back
			</button>
			 -->
		<?php if(strlen($participantId) > 0): ?>
			<div class="card">
				<div class="card-body">
					<h4>Participant Details</h4>
					<?php
			$sql = "SELECT unique_id, fname, sname, description, dob, gender FROM patients WHERE unique_id = '$participantId'";
			$result = $conn->query ( $sql );
			
			while ( $row = $result->fetch_assoc () ) {
				echo ("<div class='col-md-3'>
                                  <div class='col-md-6 float-left'><p><strong> Name: </strong>" . $row ["fname"] . " " . $row ["sname"] . "</p>");
				echo ("<p><strong> Date of Birth: </strong>" . $row ["dob"] . "</p>");
				echo ("<p><strong> Gender: </strong>" . $row ["gender"] . "</p>
                                 </div>");
				echo ("<div class='col-md-6 float-right'><p><strong> Description: </strong><br>
                                  <p>" . $row ['description'] . "</p>
                                  </div>");
				echo ("</div>");
			}
			?>
				</div>
			</div>
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#angleData">Angle</a></li>
				<li><a data-toggle="tab" href="#momentData">Moment</a></li>
				<li><a data-toggle="tab" href="#powerData">Power</a></li>
			</ul>

			<div class="tab-content">
				<div id="angleData" class="tab-pane fade in active">
					<h3>Angle Data</h3>
					<?php
			if (strlen ( $participantId ) > 0) {
				$sql = "SELECT id, unique_id, fname, sname, dob, gender, angle_file FROM patients WHERE unique_id = '$participantId'";
				$result = $conn->query ( $sql );
				
				if ($result->num_rows > 0) {
					while ( $row = $result->fetch_assoc () ) {
						getChartData ( $row, "angle_file" );
					}
					;
				}
			}
			?>			    
</div>
				<div id="momentData" class="tab-pane fade">
					<h3>Moment Data</h3>
					<?php
			if (strlen ( $participantId ) > 0) {
				$sql = "SELECT id, unique_id, fname, sname, dob, gender, moment_file FROM patients WHERE unique_id = '$participantId'";
				$result = $conn->query ( $sql );
				
				if ($result->num_rows > 0) {
					while ( $row = $result->fetch_assoc () ) {
						getChartData ( $row, "moment_file" );
					}
					;
				}
			}
			?>				    
				</div>
				<div id="powerData" class="tab-pane fade">
					<h3>Power Data</h3>
					<?php
			if (strlen ( $participantId ) > 0) {
				$sql = "SELECT id, unique_id, fname, sname, dob, gender, power_file FROM patients WHERE unique_id = '$participantId'";
				$result = $conn->query ( $sql );
				
				if ($result->num_rows > 0) {
					while ( $row = $result->fetch_assoc () ) {
						getChartData ( $row, "power_file" );
					}
					;
				}
			}
			?>				    
				</div>
			</div>
	
			<?php else: ?>
				<?php
			$sql = "SELECT timecreated, angle_file FROM patients ORDER BY timecreated DESC LIMIT 1";
			$result = $conn->query ( $sql );
			
			if ($result->num_rows > 0) {
				while ( $row = $result->fetch_assoc () ) {
					getChartData ( $row, "angle_file" );
				}
			}
			
			?>
			<?php endif; ?>
		<?php
		function getChartData($result, $type) {
			if ($result ["$type"] != null) {
				$fileContents = file_get_contents ( "patients/" . $result ["$type"] );
				
				$testArray = explode ( "\n", $fileContents );
				$testObject = ( object ) $testArray;
				
				$xChart = array ();
				$xChart1 = array ();
				$xChart2 = array ();
				$xChart3 = array ();
				$xChart4 = array ();
				$xChart5 = array ();
				$xChart6 = array ();
				$xChart7 = array ();
				$xChart8 = array ();
				$xChart9 = array ();
				
				$yChart = array ();
				$yChart1 = array ();
				$yChart2 = array ();
				$yChart3 = array ();
				$yChart4 = array ();
				$yChart5 = array ();
				$yChart6 = array ();
				$yChart7 = array ();
				$yChart8 = array ();
				$yChart9 = array ();
				
				$zChart = array ();
				$zChart1 = array ();
				$zChart2 = array ();
				$zChart3 = array ();
				$zChart4 = array ();
				$zChart5 = array ();
				$zChart6 = array ();
				$zChart7 = array ();
				$zChart8 = array ();
				$zChart9 = array ();
				
				$xChartTitle = null;
				$yChartTitle = null;
				$zChartTitle = null;
				$amountOfCharts = 0;
				$counter = 1;
				
				if ($type == "power_file" || $type == "moment_file") {
					$amountOfCharts = 3;
				} else if ($type == "angle_file") {
					$amountOfCharts = 4;
				}
				
				for($i = 0; $i < $amountOfCharts; $i ++) {
					try {
						// For each chart
						
						for($j = 5; $j < sizeof ( $testArray ) - 1; $j ++) { // Loop through each row
							$splitArr = preg_split ( '/\s+/', $testArray [$j] );
							if ($i != 0) {
								if ($type == "power_file" || $type == "moment_file") {
									array_push ( $xChart, $splitArr [$counter] . "," );
									array_push ( $xChart1, $splitArr [$counter + 9] . "," );
									array_push ( $xChart2, $splitArr [$counter + 18] . "," );
									array_push ( $xChart3, $splitArr [$counter + 27] . "," );
						
									array_push ( $yChart, $splitArr [$counter + 1] . "," );
									array_push ( $yChart1, $splitArr [$counter + 10] . "," );
									array_push ( $yChart2, $splitArr [$counter + 19] . "," );
									array_push ( $yChart3, $splitArr [$counter + 28] . "," );
						
									array_push ( $zChart, $splitArr [$counter + 2] . "," );
									array_push ( $zChart1, $splitArr [$counter + 11] . "," );
									array_push ( $zChart2, $splitArr [$counter + 20] . "," );
									array_push ( $zChart3, $splitArr [$counter + 29] . "," );
						
									$xChartTitle = $titleSplit [$counter] . $titleSplit [$counter + 1] . $titleSplit [$counter + 2];
									$yChartTitle = $titleSplit [$counter + 3] . $titleSplit [$counter + 3] . $titleSplit [$counter + 5];
									$zChartTitle = $titleSplit [$counter + 6] . $titleSplit [$counter + 7] . $titleSplit [$counter + 8];
								} else if ($type == "angle_file") {
									array_push ( $xChart, $splitArr [$counter] . "," );
									array_push ( $xChart1, $splitArr [$counter + 12] . "," );
									array_push ( $xChart2, $splitArr [$counter + 24] . "," );
									array_push ( $xChart3, $splitArr [$counter + 36] . "," );
						
									array_push ( $yChart, $splitArr [$counter + 1] . "," );
									array_push ( $yChart1, $splitArr [$counter + 13] . "," );
									array_push ( $yChart2, $splitArr [$counter + 25] . "," );
									array_push ( $yChart3, $splitArr [$counter + 37] . "," );
						
									array_push ( $zChart, $splitArr [$counter + 2] . "," );
									array_push ( $zChart1, $splitArr [$counter + 14] . "," );
									array_push ( $zChart2, $splitArr [$counter + 26] . "," );
									array_push ( $zChart3, $splitArr [$counter + 38] . "," );
						
									$xChartTitle = $titleSplit [$counter] . $titleSplit [$counter + 1] . $titleSplit [$counter + 2];
									$yChartTitle = $titleSplit [$counter + 3] . $titleSplit [$counter + 3] . $titleSplit [$counter + 5];
									$zChartTitle = $titleSplit [$counter + 6] . $titleSplit [$counter + 7] . $titleSplit [$counter + 8];
								}
							} else {
								$titleSplit = preg_split ( '/\s+/', $testArray [1] );
								$xChartTitle = $titleSplit [1] . $titleSplit [2] . $titleSplit [3];
								$yChartTitle = $titleSplit [4] . $titleSplit [5] . $titleSplit [6];
								$zChartTitle = $titleSplit [7] . $titleSplit [8] . $titleSplit [9];
									
								if ($type == "power_file" || $type == "moment_file") {
									array_push ( $xChart, $splitArr [1] . "," );
									array_push ( $xChart1, $splitArr [10] . "," );
									array_push ( $xChart2, $splitArr [19] . "," );
									array_push ( $xChart3, $splitArr [28] . "," );
									array_push ( $xChart4, $splitArr [37] . "," );
									array_push ( $xChart5, $splitArr [46] . "," );
									array_push ( $xChart6, $splitArr [55] . "," );
									array_push ( $xChart7, $splitArr [64] . "," );
									array_push ( $xChart8, $splitArr [73] . "," );
									array_push ( $xChart9, $splitArr [82] . "," );
						
									array_push ( $yChart, $splitArr [2] . "," );
									array_push ( $yChart1, $splitArr [11] . "," );
									array_push ( $yChart2, $splitArr [20] . "," );
									array_push ( $yChart3, $splitArr [29] . "," );
									array_push ( $yChart4, $splitArr [38] . "," );
									array_push ( $yChart5, $splitArr [47] . "," );
									array_push ( $yChart6, $splitArr [56] . "," );
									array_push ( $yChart7, $splitArr [65] . "," );
									array_push ( $yChart8, $splitArr [74] . "," );
									array_push ( $yChart9, $splitArr [83] . "," );
						
									array_push ( $zChart, $splitArr [3] . "," );
									array_push ( $zChart1, $splitArr [12] . "," );
									array_push ( $zChart2, $splitArr [21] . "," );
									array_push ( $zChart3, $splitArr [30] . "," );
									array_push ( $zChart4, $splitArr [39] . "," );
									array_push ( $zChart5, $splitArr [48] . "," );
									array_push ( $zChart6, $splitArr [57] . "," );
									array_push ( $zChart7, $splitArr [66] . "," );
									array_push ( $zChart8, $splitArr [74] . "," );
									array_push ( $zChart9, $splitArr [84] . "," );
								} else if ($type == "angle_file") {
									array_push ( $xChart, $splitArr [1] . "," );
									array_push ( $xChart1, $splitArr [13] . "," );
									array_push ( $xChart2, $splitArr [25] . "," );
									array_push ( $xChart3, $splitArr [37] . "," );
									array_push ( $xChart4, $splitArr [49] . "," );
									array_push ( $xChart5, $splitArr [61] . "," );
									array_push ( $xChart6, $splitArr [73] . "," );
									array_push ( $xChart7, $splitArr [85] . "," );
									array_push ( $xChart8, $splitArr [97] . "," );
									array_push ( $xChart9, $splitArr [109] . "," );
						
									array_push ( $yChart, $splitArr [2] . "," );
									array_push ( $yChart1, $splitArr [14] . "," );
									array_push ( $yChart2, $splitArr [26] . "," );
									array_push ( $yChart3, $splitArr [38] . "," );
									array_push ( $yChart4, $splitArr [50] . "," );
									array_push ( $yChart5, $splitArr [62] . "," );
									array_push ( $yChart6, $splitArr [74] . "," );
									array_push ( $yChart7, $splitArr [86] . "," );
									array_push ( $yChart8, $splitArr [98] . "," );
									array_push ( $yChart9, $splitArr [110] . "," );
						
									array_push ( $zChart, $splitArr [3] . "," );
									array_push ( $zChart1, $splitArr [15] . "," );
									array_push ( $zChart2, $splitArr [27] . "," );
									array_push ( $zChart3, $splitArr [39] . "," );
									array_push ( $zChart4, $splitArr [51] . "," );
									array_push ( $zChart5, $splitArr [63] . "," );
									array_push ( $zChart6, $splitArr [75] . "," );
									array_push ( $zChart7, $splitArr [87] . "," );
									array_push ( $zChart8, $splitArr [99] . "," );
									array_push ( $zChart9, $splitArr [111] . "," );
								}
							}
						}
							
						$xChartData = implode ( "", $xChart );
						$xChartData1 = implode ( "", $xChart1 );
						$xChartData2 = implode ( "", $xChart2 );
						$xChartData3 = implode ( "", $xChart3 );
						$xChartData4 = implode ( "", $xChart4 );
						$xChartData5 = implode ( "", $xChart5 );
						$xChartData6 = implode ( "", $xChart6 );
						$xChartData7 = implode ( "", $xChart7 );
						$xChartData8 = implode ( "", $xChart8 );
						$xChartData9 = implode ( "", $xChart9 );
							
						$yChartData = implode ( "", $yChart );
						$yChartData1 = implode ( "", $yChart1 );
						$yChartData2 = implode ( "", $yChart2 );
						$yChartData3 = implode ( "", $yChart3 );
						$yChartData4 = implode ( "", $yChart4 );
						$yChartData5 = implode ( "", $yChart5 );
						$yChartData6 = implode ( "", $yChart6 );
						$yChartData7 = implode ( "", $yChart7 );
						$yChartData8 = implode ( "", $yChart8 );
						$yChartData9 = implode ( "", $yChart9 );
							
						$zChartData = implode ( "", $zChart );
						$zChartData1 = implode ( "", $zChart1 );
						$zChartData2 = implode ( "", $zChart2 );
						$zChartData3 = implode ( "", $zChart3 );
						$zChartData4 = implode ( "", $zChart4 );
						$zChartData5 = implode ( "", $zChart5 );
						$zChartData6 = implode ( "", $zChart6 );
						$zChartData7 = implode ( "", $zChart7 );
						$zChartData8 = implode ( "", $zChart8 );
						$zChartData9 = implode ( "", $zChart9 );
							
						generateChart ( $xChartData, $xChartData1, $xChartData2, $xChartData3, $xChartData4, $xChartData5, $xChartData6, $xChartData7, $xChartData8, $xChartData9, substr ( uniqid (), 5 ) . $type, $xChartTitle );
						generateChart ( $yChartData, $yChartData1, $yChartData2, $yChartData3, $yChartData4, $yChartData5, $yChartData6, $yChartData7, $yChartData8, $yChartData9, substr ( uniqid (), 5 ) . $type, $yChartTitle );
						generateChart ( $zChartData, $zChartData1, $zChartData2, $zChartData3, $zChartData4, $zChartData5, $zChartData6, $zChartData7, $zChartData8, $zChartData9, substr ( uniqid (), 5 ) . $type, $zChartTitle );
							
						$counter = $counter + 3;
							
						$xChart = array ();
						$xChart1 = array ();
						$xChart2 = array ();
						$xChart3 = array ();
						$xChart4 = array ();
						$xChart5 = array ();
						$xChart6 = array ();
						$xChart7 = array ();
						$xChart8 = array ();
						$xChart9 = array ();
							
						$yChart = array ();
						$yChart1 = array ();
						$yChart2 = array ();
						$yChart3 = array ();
						$yChart4 = array ();
						$yChart5 = array ();
						$yChart6 = array ();
						$yChart7 = array ();
						$yChart8 = array ();
						$yChart9 = array ();
							
						$zChart = array ();
						$zChart1 = array ();
						$zChart2 = array ();
						$zChart3 = array ();
						$zChart4 = array ();
						$zChart5 = array ();
						$zChart6 = array ();
						$zChart7 = array ();
						$zChart8 = array ();
						$zChart9 = array ();
							
						$xChartTitle = null;
						$yChartTitle = null;
						$zChartTitle = null;
						
					} catch(Exception $e){
						
					}
					
				}
			} else {
				echo "No file was saved for this data!";
			}
		}
		function generateChart($data, $data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $id, $title) {
			
			echo "
				<div class='col-md-4 pull-left'>
					<br>
					<canvas id='$id-myLineGraph' width='900' height='450'></canvas>
					<br>
				</div>
			";
			
			echo "
			<script type='text/javascript'>
				var line$id = document.getElementById('$id-myLineGraph');
				var myScatterGraph$id = new Chart(line$id, {
					type : 'line',
					data : {
						labels: [$data],
						datasets : [ {
							label : '$title',
							data : [$data],
							borderWidth: 0.5,
							borderColor: '#3e95cd',
						}, {
							label : '$title',
							data : [$data1],
							borderWidth: 0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data2],
							borderWidth: 0.5,
							borderColor: 'red',
						}
						, {
							label : '$title',
							data : [$data3],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data4],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data5],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data6],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data7],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data8],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						, {
							label : '$title',
							data : [$data9],
							borderWidth:0.5,
							borderColor: 'blue',
						}
						

						]
					},
					options : {
						responsive: true,
						scales:
				        {
					      xAxes: [{
					        display: false,
					        gridLines: {
					        	display: true,
					        }
					      }], 
					      yAxes: [{
					        gridLines: {
					        	display: true,
					        }
					      }], 
				        },
					 legend: {
		 	               display: false,
		            },
                    elements: {
                        line: {
                                fill: false
                        },
                        point: { radius: 0 }
                    }
					}
				});
			</script>";
		}
		?>
		</div>
	</div>
	</div>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		function test(value){
			console.log("test");
			value.data.datasets[1].borderColor = '#f39c12';
		}
	});
	</script>
</body>
</html>