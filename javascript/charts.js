// Chart js {
window.onload = function() {
	var ctx = document.getElementById("myChart");
	var myChart = new Chart(ctx, {
		type : 'bar',
		data : {
			labels : [ "Leg", "Thigh", "Knee", "Hamstring", "Ankle", "Feet" ],
			datasets : [ {
				label : 'Gait Analysis',
				data : [ 12, 19, 3, 5, 2, 3 ],
				backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)',
						'rgba(255, 159, 64, 0.2)' ],
				borderColor : [ 'rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)' ],
				borderWidth : 1
			} ]
		},
		options : {
			scales : {
				yAxes : [ {
					ticks : {
						beginAtZero : true
					}
				} ]
			}
		}
	});

	var bub = document.getElementById("myBubbleChart");
	var myBubbleChart = new Chart(bub, {
		type : 'bubble',
		data : {
			labels : "Leg",
			datasets : [ {
				label : [ "Shin" ],
				backgroundColor : "rgba(255,221,50,0.2)",
				borderColor : "rgba(255,221,50,1)",
				data : [ {
					x : 21269017,
					y : 5.245,
					r : 15
				} ]
			}, {
				label : [ "Ankle" ],
				backgroundColor : "rgba(60,186,159,0.2)",
				borderColor : "rgba(60,186,159,1)",
				data : [ {
					x : 258702,
					y : 7.526,
					r : 10
				} ]
			}, {
				label : [ "Feet" ],
				backgroundColor : "rgba(0,0,0,0.2)",
				borderColor : "#000",
				data : [ {
					x : 3979083,
					y : 6.994,
					r : 15
				} ]
			}, {
				label : [ "Thigh" ],
				backgroundColor : "rgba(193,46,12,0.2)",
				borderColor : "rgba(193,46,12,1)",
				data : [ {
					x : 4931877,
					y : 5.921,
					r : 15
				} ]
			} ]
		},
		options : {
			title : {
				display : true,
				text : 'Gait Analysis'
			},
			scales : {
				yAxes : [ {
					scaleLabel : {
						display : true,
						labelString : "Walking"
					}
				} ],
				xAxes : [ {
					scaleLabel : {
						display : true,
						labelString : "Gait"
					}
				} ]
			}
		}
	});

	new Chart(document.getElementById("myDonutChart"), {
		type : 'doughnut',
		data : {
			labels : [ "Shin", "Thigh", "Ankle", "Foot", "Sole" ],
			datasets : [ {
				label : "Weight",
				backgroundColor : [ "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9",
						"#c45850" ],
				data : [ 2478, 5267, 734, 784, 433 ]
			} ]
		},
		options : {
			title : {
				display : true,
				text : 'Gait Analysis'
			}
		}
	});

	new Chart(document.getElementById("myRadarChart"), {
		type : 'radar',
		data : {
			labels : [ "Thigh", "Knee", "Ankle", "Feet",
					"Shin" ],
			datasets : [ {
				label : "1950",
				fill : true,
				backgroundColor : "rgba(179,181,198,0.2)",
				borderColor : "rgba(179,181,198,1)",
				pointBorderColor : "#fff",
				pointBackgroundColor : "rgba(179,181,198,1)",
				data : [ 8.77, 55.61, 21.69, 6.62, 6.82 ]
			}, {
				label : "2050",
				fill : true,
				backgroundColor : "rgba(255,99,132,0.2)",
				borderColor : "rgba(255,99,132,1)",
				pointBorderColor : "#fff",
				pointBackgroundColor : "rgba(255,99,132,1)",
				pointBorderColor : "#fff",
				data : [ 25.48, 54.16, 7.61, 8.06, 4.45 ]
			} ]
		},
		options : {
			title : {
				display : true,
				text : 'Gait Analysis'
			}
		}
	});

	new Chart(document.getElementById("lineChart"), {
		type : 'line',
		data : {
			labels : [ 1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999,
					2050 ],
			datasets : [ {
				data : [ 86, 114, 106, 106, 107, 111, 133, 221, 783, 2478 ],
				label : "Africa",
				borderColor : "#3e95cd",
				fill : false
			}, {
				data : [ 282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267 ],
				label : "Asia",
				borderColor : "#8e5ea2",
				fill : false
			}, {
				data : [ 168, 170, 178, 190, 203, 276, 408, 547, 675, 734 ],
				label : "Europe",
				borderColor : "#3cba9f",
				fill : false
			}, {
				data : [ 40, 20, 10, 16, 24, 38, 74, 167, 508, 784 ],
				label : "Latin America",
				borderColor : "#e8c3b9",
				fill : false
			}, {
				data : [ 6, 3, 2, 2, 7, 26, 82, 172, 312, 433 ],
				label : "North America",
				borderColor : "#c45850",
				fill : false
			} ]
		},
		options : {
			title : {
				display : true,
				text : 'Gait Analysis'
			}
		}
	});
}

// }
