<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/login.css?ver=0.2">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	 
	<title>Sign In</title>
</head>
<body class="main">
	<section>
		<div id="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					
						<div id="loginForm" class="loginForm">
						<div class="loginForm-header">
							<img class="logo" src="img/logo/logo.jpg"/><br>
							<p>Sign Up </p>
						</div>
						<form  class="signin-form" action="php/api/register.php" method="POST">
							
							<div id="form-errors">
						    <?php if(isset($_SESSION['Error'])):?>
						            <p class="error"><?php
									echo $_SESSION ['Error'];
									unset ( $_SESSION ['Error'] );
									session_destroy ();
									?></p>
						     <?php endif;?>
							</div>
							<input type="text" name="first_name" class="fname" placeholder="First Name" title="Enter your first name" required>
							<input type="text" name="last_name" class="lname" placeholder="Last Name" title="Enter your last name" required><br>
							<input type="text" name="username" class="username" placeholder="Username" title="university username" required><br>
			        		<input type="password" class="password" name="password"  placeholder="Password" title="Please create a password" required><br>
							<input type="password" class="password" name="password2"  placeholder="Re-enter Password" title="Please Re-enter your password" required><br>
							<input type="submit" id="loginBtn" value=" Sign Up">
							<label>Already have an account? <a class="signupBtn" href="login.php">Sign In</a></label>
						</form>
						</div>
					<div class="col-md-4"></div>
		</div>
	</section>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</body>
</html>