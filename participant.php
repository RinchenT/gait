<?php
session_start ();
include 'php/api/check.php';
include 'php/api/main/upload.php';


?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/index.css?'?ver=0.3'">
<link rel="stylesheet" type="text/css" href="css/navbar.css?'?ver=0.7'">
<link rel="stylesheet" type="text/css" href="css/patients.css?ver=0.2'">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<title>Participants</title>
</head>
<body>
	
   <?php include 'php/navbar.php';?>
   <!DOCTYPE html>

	<div id="main">
		<div class="main-head">
			<a href="index.php">Dashboard</a> / Participant
		</div>
		<div class="container-wrap">
			<div class="participants">
				<div class="paricipants-header">
					<a class="addBtn" href="add-participant.php"><i class="fa fa-fw  fa-plus"></i>Add participant</a>
					<form class="search-form" action="get-participants.php" method="POST">
						<input type="text" name="search"  class="searchBar" placeholder="Search..."/>
						<button type="submit" class="searchSubmit"><i class="fa fa-fw fa-search"></i></button>
					</form>
				</div>

				<table class="table"> 
				<thead>
					<tr>
						<th scope="col">Id No</th>
						<th scope="col">First Name</th>
						<th scope="col">Last Name</th>
						<th scope="col">DoB</th>
						<th scope="col">Gender</th>
						<th scope="col">Chart</th>
					</tr>
				</thead>
				<tbody>
   <?php 
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "gait";
			
			$conn = new mysqli($servername, $username, $password, $dbname);
			

			$sql = "SELECT id, unique_id, fname, sname, dob, gender, angle_file FROM patients";
			$result = $conn->query($sql);
			
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$patientId = $row["unique_id"];
					echo "<tr>";
					echo "<td>" . $row["id"]. "</td><td>" . $row["fname"]. "</td><td>" . $row["sname"].
					"</td><td>" . $row["dob"]. "</td><td>" . $row["gender"]. 
					"</td><td><a href='charts.php?$patientId'><i class='fa fa-area-chart'> </i> </a></td>";
				}
			} else {
				echo "<tr> <td> 0 results </td> </tr>";
			}
?>
  				</tbody>
			</table>

			</div>
	</div>
</body>
<script>

</script>
</html>