<?php
session_start ();
include 'php/api/check.php';
include 'php/api/main/upload.php';


?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/index.css?'?ver=0.3'">
<link rel="stylesheet" type="text/css" href="css/navbar.css?'?ver=0.7'">
<link rel="stylesheet" type="text/css" href="css/profile.css?ver=0.1'">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<title>Participants</title>
</head>
<body>
	
   <?php include 'php/navbar.php';?>
   <!DOCTYPE html>

	<div id="main">
		<div class="profile_wrap">
			<div class="row">
    			<div class="col-md-3">
    				<div class="profile_pic">
    					<img src="img/profile-pic/profile-pic.jpg"/>
    				</div>
    			</div>
    			<div class="col-md-5" >
    				<div class="profile-info">
    					<p><b>Contact Information</b></p>
    					<p>E-Mail: &nbsp;<?= $_SESSION['user']['email']?></p></p><br>
    					<p><b>General Information</b></p>
    					<p>First Name: &nbsp;<?= $_SESSION['user']['first_name']?></p>
    					<p>Last Name: &nbsp;<?= $_SESSION['user']['last_name']?></p>
    					<p>Position: &nbsp;<?= $_SESSION['user']['position']?></p>
    				</div>
    			</div>
    			<div class="col-md-3">
    				<div class="edit_Account">
    					<p><b>Account Settings</b></p>
    					<a href="edit-account.php">Edit Account</a>
    				</div>
    			</div>
			</div>
		</div>
	</div>
	
</body>
</html>