<?php
session_start ();
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/login.css?ver=0.4">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Sign In</title>
</head>
<body class="main">
	<section>
		<div id="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					
						<div id="loginForm" class="loginForm">
						<div class="loginForm-header">
							<img class="logo" src="img/logo/logo.jpg"/><br>
							Sign In 
						</div>
						<form  class="signin-form" action="php/api/process.php" method="POST">
							
							<div id="form-errors">
						    <?php if(isset($_SESSION['Error'])):?>
						            <p class="error"><?php
									echo $_SESSION ['Error'];
									unset ( $_SESSION ['Error'] );
									session_destroy ();
									?></p>
						     <?php endif;?>
							</div>
							<div id="form-approved">
						    <?php if(isset($_SESSION['Approved'])):?>
						            <p class="error"><?php
									echo $_SESSION ['Approved'];
									unset ( $_SESSION ['Approved'] );
									session_destroy ();
									?></p>
				     		<?php endif;?>
				     		</div>
							<input type="text" class="username" name="username" placeholder="Username"><br>
							<input type="password" class="password" name="password" placeholder="Password"><br>
							<input type="submit" id="loginBtn" value=" Sign In">
							<label>Dont have an account? <a class="signupBtn" href="register.php">Sign Up</a></label>
						</form>
						</div>
						<div class="info-help">
							<p><i class="fa fa-info-circle"></i> <b>Sign in with your ljmu.ac.uk username.</b></p>
						</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</div>
		</div>
	</section>

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</body>
</html>